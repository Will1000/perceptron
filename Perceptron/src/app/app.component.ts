import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Entrenamiento } from './models/entrenamiento';
import { Neuron } from './models/neuron';
import { GraficResultService } from './services/grafic-result.service';
import { LocalstoreService } from './services/localstore.service';
import { seleccionarOpcion } from './validators/funcion_activacion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

}

