import { Injectable } from '@angular/core';
import { Entrenamiento } from '../models/entrenamiento';
import { Neuron } from '../models/neuron';

@Injectable({
  providedIn: 'root'
})
export class EntrenamientoService {

  constructor() { }


  calcularErrorERM(errorPatron: number[], nPatrones: number): number {
    let errorRMS: number = 0;

   console.log(errorPatron);
    for (let i = 0; i < errorPatron.length; i++) {
      errorRMS += errorPatron[i] / nPatrones;
    }

    console.log("Error RMS: "+errorRMS);
    return errorRMS;

  }


  ajustarPesos(patron: number, salidas: number[][], neurona: Neuron, vectorPesos: any[], entrenamiento: Entrenamiento) {
    let row: number[] = [];
    let x: number[] = []; // patron
    let matrizPesos: number[][];
    let newMatrizPeso: number[][] = [];
    let newVectorPesos = [];

    x = neurona.datos[patron];

    for (let capas = 0; capas < salidas.length; capas++) {

      matrizPesos = [];
      newMatrizPeso = [];
      matrizPesos = vectorPesos[capas];
      let index = capas + 1;

      let iFin = salidas[capas].length;
      let jFin;
      if (capas == 0) {
        jFin = neurona.nEntradas; //2
      }else {
        jFin = x.length; //
      }

      for (let i = 0; i < jFin; i++) {

        row = [];
        for (let j = 0; j <iFin ; j++) {

          if (index != (salidas.length)) {
            console.log("W"+(j+1)+""+(i+1)+": "+ matrizPesos[i][j]+" + "+entrenamiento.rAprendizaje+" * "+entrenamiento.errorPatron[patron]+" * "+ x[i]);
            let value = (matrizPesos[i][j])+ (entrenamiento.rAprendizaje * entrenamiento.errorPatron[patron] * x[i]);
            value = parseFloat((value).toFixed(5));
            row.push(value);
          }else {
            console.log("W"+(j+1)+""+(i+1)+": "+ matrizPesos[i][j]+" + "+entrenamiento.rAprendizaje+" * "+entrenamiento.errorLineal[j]+" * "+ x[i]);
            let value = (matrizPesos[i][j])+ (entrenamiento.rAprendizaje * entrenamiento.errorLineal[j] * x[i]);
            value = parseFloat((value).toFixed(5));
            row.push(value);
          }

        }
        newMatrizPeso.push(row);
      }
      newVectorPesos.push(newMatrizPeso);
      x = salidas[capas];

    }

    return newVectorPesos;
  }

  ajustarUmbrales(umbrales: number[][], entrenamiento: Entrenamiento, patron: number) {
    let newUmbrales: number[] = [];
    let newMarizUmbral: number[][] = [];
    let vectorUmbral: number[];
    let x0: number = 1;
    let index: number;

    for (let umbral = 0; umbral < umbrales.length; umbral++) {

      newUmbrales = [];
      vectorUmbral = [];
      vectorUmbral = umbrales[umbral];
      console.log(vectorUmbral);
      index = umbral + 1;

      for (let i = 0; i < vectorUmbral.length; i++) {

        if (index != (umbrales.length)) {

          let value =(vectorUmbral[i]) + (entrenamiento.rAprendizaje * entrenamiento.errorPatron[patron] * x0);
          value = parseFloat((value).toFixed(5));
          newUmbrales.push(value);

        }else {
          let value = (vectorUmbral[i]) + (entrenamiento.rAprendizaje * entrenamiento.errorLineal[i] * x0);
          value = parseFloat((value).toFixed(5));
          newUmbrales.push(value);
        }

      }
      newMarizUmbral.push(newUmbrales);
    }

    return newMarizUmbral;
  }

  salidaRed(salida: number,fActivacion: string): number {

    if (fActivacion === "Tangente") {
      return parseFloat(Math.tanh(salida).toFixed(5));
    }else if (fActivacion === "Lineal") {
      return parseFloat(salida.toFixed());
    }else if (fActivacion === "Sigmoide") {
      return parseFloat((1 / (1 + Math.pow(Math.E,-salida))).toFixed(5));
    }

  }

  obtenersalidas(patron: number[], nEntradas: number): number[] {
    let salidas: number[] = [];
    for (let i = nEntradas; i < patron.length; i++) {
      salidas.push(patron[i]);
    }
    console.log("Salidas (Yd1, Yd2): "); console.log(salidas);
    return salidas;
  }

  inicializarPesos(neurona: Neuron, capOcultas: any): number[][] {
    let nMatices = capOcultas.length+1;
    let matrizPesos: number[][] = [];
    let vectorPesos = [];
    let row: number[] = [];
    let x: number[] = [];
    let ifin = neurona.nEntradas;
    let jfin;

    for (let capa = 0; capa < nMatices ; capa++) {

      matrizPesos = [];

      if(capa+1 !== nMatices) {
        jfin = capOcultas[capa]['nNeuronas'];
      }else {
        jfin = neurona.nSalidas;
      }

      for (let i = 0; i < ifin; i++) { // 2 3 2 3
        row = [];
        for (let j = 0; j < jfin; j++) { // 3 2 3 2
          row.push(parseFloat((this.getRandomArbitrary(-1,1)).toFixed(5)));
        }
        matrizPesos.push(row);
      }

      ifin = matrizPesos[0].length;
      vectorPesos.push(matrizPesos);
    }
    return vectorPesos;
  }

  getRandomArbitrary(min:number, max: number) {
    return Math.random() * (max - min) + min;
  }

  inicializarUnbrales(nCampos: number): number[] {
    let umbral: number[] = [];

    for (let i = 0; i < nCampos; i++) {
      umbral.push(parseFloat((this.getRandomArbitrary(-1,0)).toFixed(5)));
    }
    return umbral;
  }

  pruebaUmbrales() {
    let umbrales: number[][] = [];

    umbrales.push([-1,1,0]);
    umbrales.push([1,0]);
    umbrales.push([-1,1]);

    return umbrales;
  }



}
