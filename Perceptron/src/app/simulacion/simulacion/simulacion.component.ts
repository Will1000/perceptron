import { OnInit } from '@angular/core';
import { Entrenamiento } from 'src/app/models/entrenamiento';
import { Neuron } from 'src/app/models/neuron';
import { LocalstoreService } from 'src/app/services/localstore.service';
import {Component,ViewChild} from '@angular/core';
import {MatTable} from '@angular/material/table';
import { Router } from '@angular/router';
import { GraficResultService } from 'src/app/services/grafic-result.service';

export interface PeriodicElement {
  index: number;
  entradas: string;
  sDeseada: string;
  sObtenida: string;
}


@Component({
  selector: 'app-simulacion',
  templateUrl: './simulacion.component.html',
  styleUrls: ['./simulacion.component.css']
})
export class SimulacionComponent implements OnInit {

  data: PeriodicElement[] = [];

  displayedColumns: string[] = ['index','entradas', 'sDeseada', 'sObtenida'];
  dataSource = [...this.data];

  @ViewChild(MatTable, {static: false}) table: MatTable<PeriodicElement>;


  fileContent: string;
  neurona: Neuron;
  entrenamiento: Entrenamiento;
  matrizPesos: number[][] = [];
  umbrales: number[][] = [];
  sObtenidas: number[] = [];
  vectorPesos = [];
  capOcultas: any = [];
  matrizSalidas: number[][] = [];

  constructor(
    private localstoreService: LocalstoreService,
    private router: Router,
    private graficResultService: GraficResultService,
    ) { }

  ngOnInit(){}

  goEntrenar() {
    this.router.navigate(['/']);
  }

  addFila(fila: any) {
    this.dataSource.push(fila);
    this.table.renderRows();
  }

  public onChange(fileList: FileList): void {
    this.neurona = new Neuron();
    this.entrenamiento = new Entrenamiento();

    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    fileReader.readAsText(file);

    let self = this;
    fileReader.onload = e => {

      self.fileContent = fileReader.result as string;
      var rows = self.fileContent.split('\r\n');
      this.neurona.nSalidas = rows[0].split(';').length -1;
      this.neurona.nEntradas = rows[0].split(',').length;


      this.neurona.datos = [];
      for (var i = 1; i < rows.length; i++) {
        if (rows[i].length != 0) {
          this.neurona.datos.push(rows[i].split(',').map(Number));
        }
      }
      this.neurona.nPatrones = this.neurona.datos.length;
      console.log("Entradas: "+this.neurona.nEntradas);
      console.log("Salidas: "+this.neurona.nSalidas);
      console.log("Patrones: "+this.neurona.nPatrones)
      console.log(this.neurona.datos);

    };
  }

  getTamanoFila(index: number) {
    if (index != (this.capOcultas.length+1)) {
      return this.capOcultas[index-1]['nNeuronas'];
    }else {
      return this.neurona.nSalidas;
    }

  }

  iniciarGrafica() {
    this.dataGrafic[0].series.push({name:0, value: 0});
    this.dataGrafic[1].series.push({name:0, value: 0});
  }

  iniciarEntrenamiento() {

    this.entrenamiento.fActivacion = this.localstoreService.get('fActivacionS');
    this.vectorPesos = this.localstoreService.get('pesos');
    this.umbrales = this.localstoreService.get('umbrales')
    this.capOcultas = this.localstoreService.get('fActivacionC');

    console.log(this.vectorPesos);
    console.log(this.umbrales);
    console.log(this.capOcultas);

    this.entrenamiento.errorPatron = [];
    let x: number[] = [];
    let umbral: number[];
    let s: number;

    for (let patron = 0; patron < this.neurona.nPatrones; patron++) {

      console.log("-------PATRON "+patron+"---------");
      x = this.neurona.datos[patron];
      console.log(x);
      this.matrizSalidas = [];

      for (let capas = 0; capas < this.capOcultas.length+1; capas++) {

        this.neurona.salidaSoma  = [];
        this.matrizPesos = [];
        this.matrizPesos = this.vectorPesos[capas];
        umbral = this.umbrales[capas];

        let iFin = this.getTamanoFila(capas+1); // 4 3 3
        let jFin;
        if (capas == 0) {
          jFin = this.neurona.nEntradas; // 3
        }else {
          jFin = x.length; // 4 3
        }

        for (let i = 0; i < iFin ; i++) { // 4 3 3
          s = 0;
          for (let j = 0; j < jFin; j++) { // 3 4 3
            console.log("X"+j+":  "+x[j]);
            console.log("W"+j+""+i+": "+this.matrizPesos[j][i])
            s += (x[j]*this.matrizPesos[j][i]);
          }
          s = s - umbral[i];
          if (capas+1 !== this.capOcultas.length+1) {
            s = this.salidaRed(s, this.capOcultas[capas]['fActivacion']);
          }else {
            s = this.salidaRed(s, this.entrenamiento.fActivacion);
          }

          this.neurona.salidaSoma.push(parseFloat(s.toFixed(5)));
        }

        x = this.neurona.salidaSoma;
        this.matrizSalidas.push(x);
        console.log("Salida de capa: ");
        console.log(x);

      }

      x = [];
      x = this.obtenersalidas(this.neurona.datos[patron],this.neurona.nEntradas);

      console.log("Salidas YR: "); console.log(this.neurona.salidaSoma);
      console.log("Slidas YD: "); console.log(x);

      this.entrenamiento.errorLineal = [];
      for (let i = 0; i < this.neurona.nSalidas; i++) {

        this.sObtenidas.push(this.neurona.salidaSoma[i]);
      }

      for (let i = 0; i < this.neurona.nSalidas; i++) {

        this.dataGrafic[0].series.push({name:i+1, value: this.sObtenidas[i]});
        this.dataGrafic[1].series.push({name:i+1, value: x[i]});

      }

    }

    this.graficResultService.addGraficSimu(this.dataGrafic);
    console.log(this.graficResultService.resultGraficSimu);
  }

  salidaRed(salida: number,fActivacion: string): number {

    if (fActivacion === "Tangente") {
      return parseFloat(Math.tanh(salida).toFixed(5));
    }else if (fActivacion === "Lineal") {
      return parseFloat(salida.toFixed());
    }else if (fActivacion === "Sigmoide") {
      return parseFloat((1 / (1 + Math.pow(Math.E,-salida))).toFixed(5));
    }

  }

  obtenersalidas(patron: number[], nEntradas: number): number[] {
    let salidas: number[] = [];
    for (let i = nEntradas; i < patron.length; i++) {
      salidas.push(patron[i]);
    }
    console.log("Salidas (Yd1, Yd2): "); console.log(salidas);
    return salidas;
  }

  main() {

    this.iniciarGrafica();
    this.iniciarEntrenamiento();

    let salidasObtenidas = this.ordenarSalidas(this.sObtenidas);

    let patron: number[] = [];
    for (let i = 0; i < this.neurona.nPatrones; i++) {
      patron = this.neurona.datos[i];
      this.addFila({index: (i+1),entradas: this.obtenerEntradas(patron), sDeseada: this.obtenersalidas(patron,this.neurona.nEntradas).toString(), sObtenida: salidasObtenidas[i].toString()})
    }

  }

  ordenarSalidas(arraySObtenidas: number[]): number[][] {
    let iterador: number = 1;
    let sObtenidas: number[][] = [];
    let row: number[] = [];
    for (let i = 0; i < arraySObtenidas.length; i++) {

      row.push(arraySObtenidas[i]);
      if (this.neurona.nSalidas == iterador) {
        sObtenidas.push(row);
        row = [];
        iterador = 0;
      }

      iterador++;

    }

    return sObtenidas;
  }

  obtenerEntradas(patron: number[]): string {
    let entradas: number[] = [];
    for (let i = 0; i < this.neurona.nEntradas; i++) {
      entradas.push(patron[i]);
    }
    console.log("Entradas: "); console.log(entradas);
    return entradas.toString();
  }


    //------------------------------------------Grafica ------------------------


    view: any[] = [700, 300];

    // options
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'interaciones';
    yAxisLabel: string = 'YR';
    timeline: boolean = true;

    colorScheme = {
      domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    };



    onSelect(data): void {
      console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
      console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
      console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }


    // --------------Data ------------------------

    dataGrafic = [
      {
        name: 'YR',
        series: [
        ],
      },
      {
        name: 'YD',
        series: [
        ],
      }
    ];

    get multi() {
      return this.graficResultService.resultGraficSimu;
    }



}
