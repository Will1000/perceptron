import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { EntrenamientoComponent } from './entrenamiento/entrenamiento/entrenamiento.component';
import { SimulacionComponent } from './simulacion/simulacion/simulacion.component';

const routes: Routes = [
  { path: '', component: EntrenamientoComponent },
  { path: 'simulacion', component: SimulacionComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
