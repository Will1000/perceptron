import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SimulacionComponent } from './simulacion/simulacion/simulacion.component';
import { EntrenamientoComponent } from './entrenamiento/entrenamiento/entrenamiento.component';


@NgModule({
  declarations: [
    AppComponent,
    SimulacionComponent,
    EntrenamientoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
  ],
  entryComponents: [
    EntrenamientoComponent
 ],
  bootstrap: [AppComponent]

})
export class AppModule { }
