export class Entrenamiento {
  nIteraciones: number;
  rAprendizaje: number;
  errorMaximo: number;
  fActivacion: string
  aEntrenamiento: string;
  tipoInitPesos: string;
  errorLineal: number[];
  errorPatron: number[];
  errorRMS: number;


}
