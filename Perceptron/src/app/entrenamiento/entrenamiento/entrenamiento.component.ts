import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Entrenamiento } from 'src/app/models/entrenamiento';
import { Neuron } from 'src/app/models/neuron';
import { GraficResultService } from 'src/app/services/grafic-result.service';
import { LocalstoreService } from 'src/app/services/localstore.service';
import {MatSnackBar} from '@angular/material';
import { EntrenamientoService } from 'src/app/services/entrenamiento.service';


@Component({
  selector: 'app-entrenamiento',
  templateUrl: './entrenamiento.component.html',
  styleUrls: ['./entrenamiento.component.css']
})
export class EntrenamientoComponent implements OnInit {

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private graficResultService: GraficResultService,
    private localStoreService: LocalstoreService,
    private router: Router,
    private entrenamientoService: EntrenamientoService
    ) {

  }


  form: FormGroup;
  fileContent: string;
  neurona: Neuron;
  entrenamiento: Entrenamiento;
  matrizPesos: number[][] = [];
  vectorPesos = [];
  umbrales: number[][] = [];
  isEntrenada: boolean = false;
  countFilas: number = 0;
  row: number[] = [];
  disable: boolean = true;
  setearPesos: boolean = false;
  tamanoPesos: number[] = [];
  capOcultas: any = [];
  iterador: number = 1;
  nameButtonPesos: string = 'Siguiente';
  valueCapas: number[] = [];
  matrizSalidas: number[][] = [];


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nIteraciones: [
        '',
        {
          validators: [Validators.required, Validators.min(0), Validators.max(1000)],
        }
      ],
      rAprendizaje: [
        '',
        {
          validators: [Validators.required, Validators.min(0), Validators.max(1)],
        }
      ],
      errorMaximo: [
        '',
        {
          validators: [Validators.required, Validators.min(0), Validators.max(1)]
        }
      ],
      fActivacion: [
        '',
        {
          validators: [Validators.required],

        }
      ],
      aEntrenamiento: [
        '',
        {
          validators: [Validators.required],
        }
      ],
      tipoInitPesos: [
        '',
        {
          validators: [Validators.required],
        }
      ],
      arrayPesos: this.formBuilder.array([]),
      capasOcultas: this.formBuilder.array([])
    });
  }

  // Validaciones

  getErrorComboTipoInitPesos() {
    var field = this.form.get('tipoInitPesos');

    if (field.hasError('required')) {
      return 'El campo Tipo de inicializacion de pesos es requerido';
    }
  }

  getErrorComboAEntrenamiento() {
    var field = this.form.get('aEntrenamiento');

    if (field.hasError('required')) {
      return 'El campo Algoritmo de entrenamiento es requerido';
    }
  }

  getErrorComboFActivacion() {
    var field = this.form.get('fActivacion');

    if (field.hasError('required')) {
      return 'El campo Funcion de activacion es requerido';
    }
  }
  getErroIteraciones() {
    var field = this.form.get('nIteraciones');

    if (field.hasError('required')) {
      return 'El campo Numero de itereciones es requerido';
    }

    if (field.hasError('min')) {
      return 'El campo Numero de itereciones debe ser de minimo 0';
    }
    if (field.hasError('max')) {
      return 'El campo Numero de itereciones debe ser de maximo 1000';
    }
    return '';
  }
  getErrorErrorMaximo() {
    var field = this.form.get('errorMaximo');

    if (field.hasError('required')) {
      return 'El campo Error maximo permitido es requerido';
    }

    if (field.hasError('min')) {
      return 'El campo Error maximo permitido  debe ser de minimo 0';
    }
    if (field.hasError('max')) {
      return 'El campo Error maximo permitido  debe ser de maximo 1';
    }
    return '';
  }

  getErroIRAprendizaje() {
    var field = this.form.get('rAprendizaje');

    if (field.hasError('required')) {
      return 'El campo Rata de aprendizaje es requerido';
    }

    if (field.hasError('min')) {
      return 'El campo Rata de aprendizaje debe ser de minimo 0';
    }
    if (field.hasError('max')) {
      return 'El campo Rata de aprendizaje debe ser de maximo 1';
    }
    return '';
  }

  getErroArrayPesos(field: any) {

    if (field.hasError('required')) {
      return 'Campo requerido';
    }
    if (field.hasError('min')) {
      return 'El valor minimo debe ser -1';
    }
    if (field.hasError('max')) {
      return 'El valor maximo debe ser 1';
    }

    return '';
  }

  getErrorInputCapaOculta(field: any) {

    if (field.hasError('required')) {
      return 'El Campo es requerido';
    }
    if (field.hasError('min')) {
      return 'El valor minimo debe ser 1';
    }
    return '';
  }

  getErrorComboCapaOculta(field: any) {

    if (field.hasError('required')) {
      return 'El campo es requerido';
    }
  }
  // Metodos get

  get multi() {
    return this.graficResultService.resultGrafic;
  }

  get arrayPesos() {
    return this.form.get('arrayPesos') as FormArray;
  }

  get capasOcultas() {
    return this.form.get('capasOcultas') as FormArray;
  }


  get isInvalidParametrosEntrada() {
    return this.form.get('nIteraciones').invalid ||
    this.form.get('rAprendizaje').invalid || this.form.get('errorMaximo').invalid;
  }

  get isInvalidConfigRed() {
    return this.form.get('fActivacion').invalid ||
    this.form.get('aEntrenamiento').invalid;
  }

  get PesosValidos() {
    return this.form.get('arrayPesos').invalid;
  }

  get PesosCompletos() {

    return (this.vectorPesos.length !== this.capOcultas.length + 1);

    // if (this.form.get('tipoInitPesos').value === 'ingresarPesos') {
    //   return this.form.get('arrayPesos').invalid;
    // }
    // if (this.form.get('tipoInitPesos').value === '') {
    //   return true;
    // }
    // if (this.form.get('tipoInitPesos').value !== 'ingresarPesos') {
    //   console.log(this.vectorPesos.length !== this.capOcultas.length + 1);
    //   return this.vectorPesos.length !== this.capOcultas.length + 1;
    // }
  }

  // Cargar archivos

  public onChange(fileList: FileList): void {
    this.neurona = new Neuron();
    this.entrenamiento = new Entrenamiento();

    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    fileReader.readAsText(file);

    let self = this;
    fileReader.onload = e => {

      self.fileContent = fileReader.result as string;
      var rows = self.fileContent.split('\r\n');
      this.neurona.nSalidas = rows[0].split(';').length -1;
      this.neurona.nEntradas = rows[0].split(',').length;


      this.neurona.datos = [];
      for (var i = 1; i < rows.length; i++) {
        if (rows[i].length != 0) {
          this.neurona.datos.push(rows[i].split(',').map(Number));
        }
      }
      this.neurona.nPatrones = this.neurona.datos.length;
      console.log('umbrales: '); console.log(this.umbrales);
      console.log(this.neurona.datos);


    };
  }



  mostarPesosxxx() {
    // console.log(this.form.get('arrayPesos').value);
    // this.ordenarPesos(this.form.get('arrayPesos').value);
    // console.log(this.matrizPesos);
  }
  mostrarCapas() {

    // this.capOcultas = this.form.get('capasOcultas').value;
    // console.log(this.capOcultas);
    // console.log(this.capOcultas.length);
    // this.crearTamanoPesos();
    // console.log(this.tamanoPesos);

    console.log(this.vectorPesos);
    console.log(this.umbrales);

  }


  cargarPesos(fileList: FileList) {

    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    fileReader.readAsText(file);

    let self = this;
    fileReader.onload = e => {
      self.fileContent = fileReader.result as string;

      var rows = self.fileContent.split('\r\n');

      for (var i = 0; i < rows.length; i++) {
        if (rows[i].length != 0) {
          this.matrizPesos.push(rows[i].split(',').map(Number));
        }
      }
      console.log(this.matrizPesos);

    }
   }

  // Otros metodos

  addInputPesos() {
   this.arrayPesos.push(this.formBuilder.control('', [Validators.required, Validators.min(-1), Validators.max(1)]));
  }

  addCapaOculta() {

    const capaOcultaFomrGroup =  this.formBuilder.group({
      fActivacion: [
        '',
        {
          validators: [Validators.required],
        }
      ],
      nNeuronas: [
        '',
        {
          validators: [Validators.required, Validators.min(1)],
        }
      ],
    });
    if (this.capasOcultas.length < 5) {
      this.capasOcultas.push(capaOcultaFomrGroup);
    }
  }

  removCapaOculta(index: number) {
    this.capasOcultas.removeAt(index);
  }

  guardarNeurona(message: string, action: string) {
    this.localStoreService.set("pesos", this.vectorPesos);
    this.localStoreService.set("fActivacionS", this.entrenamiento.fActivacion);
    this.localStoreService.set("fActivacionC", this.capOcultas);
    this.localStoreService.set("umbrales", this.umbrales);
    this._snackBar.open(message, action);
  }

  getTamanoFila(index: number) {
    if (index != (this.capOcultas.length+1)) {
      return this.capOcultas[index-1]['nNeuronas'];
    }else {
      return this.neurona.nSalidas;
    }

  }

  umbralesAlAzar(index: number) {
      this.umbrales.push(this.entrenamientoService.inicializarUnbrales(this.getTamanoFila(index)));
  }


  nextMatrizPesos() {
    this.ordenarPesos(this.form.get('arrayPesos').value, this.getTamanoFila(this.iterador));
    this.limpiarPesos();
    this.umbralesAlAzar(this.iterador);
    if (this.iterador != this.capOcultas.length + 1) {
      this.setArrayPesos(this.tamanoPesos[this.iterador]);
      this.iterador++;
      if(this.iterador == this.capOcultas.length + 1) { this.nameButtonPesos = 'Finalizar'}
    }

  }

  nextMatrizPesosAzar() {
    this.ordenarPesos(this.form.get('arrayPesos').value, this.getTamanoFila(this.iterador));
  }

  limpiarPesos() {
    let tamano = this.arrayPesos.length -1;
    for (let i = tamano; i >= 0; i--) {
      this.arrayPesos.removeAt(i);
    }
  }

 setArrayPesos(valor: number) {
   for (let i = 0; i < valor; i++) {
    this.addInputPesos();
   }
 }


 getArray(valor: number): number[] {
  let array: number[] = [];
  for (let i = 0; i < valor; i++) {
   array.push(i);
  }
  return array;
}

  iniciarGrafica() {
    this.data[0].series.push({name: 0, value: 0});
  }

  goSimulacion() {
    this.router.navigate(['/simulacion']);
  }

  crearTamanoPesos() {
    this.tamanoPesos.push(this.neurona.nEntradas * this.capOcultas[0]['nNeuronas']);

    if(this.capOcultas.length > 1) {
      for (let i = 0; i < this.capasOcultas.length-1; i++) {
        this.tamanoPesos.push(this.capOcultas[i]['nNeuronas'] * this.capOcultas[i+1]['nNeuronas']);
      }
    }

    this.tamanoPesos.push(this.capOcultas[this.capOcultas.length -1]['nNeuronas'] * this.neurona.nSalidas);
  }

  selectOpcion(value: any) {

    this.matrizPesos = [];
    this.arrayPesos.clear();
    this.setearPesos = false;
    this.vectorPesos = [];
    this.capOcultas = [];
    if (value === 'alAzar') {

      this.capOcultas = this.form.get('capasOcultas').value;
      this.vectorPesos = this.entrenamientoService.inicializarPesos(this.neurona, this.capOcultas);

      for (let capa = 0; capa < this.capasOcultas.length+1; capa++) {
        this.umbrales.push(this.entrenamientoService.inicializarUnbrales(this.getTamanoFila(capa+1)));

      }

    } else if (value === 'cargarPesos') {
      //
    }else {
      this.setearPesos = true;
      this.capOcultas = this.form.get('capasOcultas').value;
      this.tamanoPesos = [];
      this.crearTamanoPesos();
      this.setArrayPesos(this.tamanoPesos[0]);
    }

    console.log(this.matrizPesos);

  }

  ordenarPesos(arrayPesos: number[], tamFila: number) {
    this.matrizPesos = [];
    let iterador: number = 1;
    let row: number[] = [];
    for (let i = 0; i < arrayPesos.length; i++) {

      row.push(arrayPesos[i]);
      if (tamFila == iterador) {
        this.matrizPesos.push(row);
        row = [];
        iterador = 0;
      }

      iterador++;
    }

    this.vectorPesos.push(this.matrizPesos);
  }

  addPesos(e: any) {
    if (e !== '') {
      this.row.push(parseFloat(e));
      this.countFilas++; // 1
      if (this.neurona.nSalidas == this.countFilas ) {
        this.matrizPesos.push(this.row);
        this.row = [];
        this.countFilas = 0;
      }
      console.log(this.matrizPesos);
    }

    if (this.matrizPesos.length == (this.neurona.nEntradas * this.neurona.nSalidas)) {
      this.disable = false;
    }

  }

  main(){

    this.entrenamiento = this.form.value;
    console.log(this.entrenamiento);
    let primeraIteracion: boolean = true;
    let i: number = 1;
    this.iniciarGrafica();
    while(i <= this.entrenamiento.nIteraciones  && this.isEntrenada == false ) {

      this.iniciarEntrenamiento();

      this.data[0].series.push({name: i, value: this.entrenamiento.errorRMS});

      if (this.entrenamiento.errorRMS <= this.entrenamiento.errorMaximo) {
        this.isEntrenada = true;
      }
      i++;
      // primeraIteracion = false;
      console.log("iteraciones: "+this.entrenamiento.nIteraciones);

    }
    this.graficResultService.addGrafic(this.data);
    console.log(this.graficResultService.resultGrafic);
  }




  iniciarEntrenamiento() {
    // this.umbrales = [];
    // this.umbrales = this.entrenamientoService.pruebaUmbrales();
    console.log(this.vectorPesos);
    console.log(this.entrenamiento);
    console.log(this.umbrales);

    this.entrenamiento.errorPatron = [];
    let x: number[] = [];
    let umbral: number[] = [];
    let s: number;

    for (let patron = 0; patron < this.neurona.nPatrones; patron++) {

      console.log("-------PATRON "+patron+"---------");
      x = this.neurona.datos[patron];
      console.log(x);
      this.matrizSalidas = [];


      for (let capas = 0; capas < this.capOcultas.length+1; capas++) {

        this.neurona.salidaSoma  = [];
        this.matrizPesos = [];
        this.matrizPesos = this.vectorPesos[capas];
        umbral = this.umbrales[capas];
        let iFin = this.getTamanoFila(capas+1); // 4 3 3
        let jFin;
        if (capas == 0) {
          jFin = this.neurona.nEntradas; // 3
        }else {
          jFin = x.length; // 4 3
        }


        for (let i = 0; i < iFin ; i++) { // 4 3 3
          s = 0;
          for (let j = 0; j < jFin; j++) { // 3 4 3
            console.log("X"+j+":  "+x[j]);
            console.log("W"+j+""+i+": "+this.matrizPesos[j][i])
            s += (x[j]*this.matrizPesos[j][i]);
          }
          s = s - umbral[i];
          if (capas+1 !== this.capOcultas.length+1) {
            s = this.entrenamientoService.salidaRed(s, this.capOcultas[capas]['fActivacion']);
          }else {
            s = this.entrenamientoService.salidaRed(s, this.entrenamiento.fActivacion);
          }

          this.neurona.salidaSoma.push(s);
        }

        x = this.neurona.salidaSoma;
        this.matrizSalidas.push(x);
        console.log("Salida de capa: ");
        console.log(x);

      }
      x = [];
      x = this.entrenamientoService.obtenersalidas(this.neurona.datos[patron],this.neurona.nEntradas);

      console.log("Salidas YR: "); console.log(this.neurona.salidaSoma);
      console.log("Slidas YD: "); console.log(x);

      this.entrenamiento.errorLineal = [];
      for (let i = 0; i < this.neurona.nSalidas; i++) {

        // console.log("Salida red (YR"+(i)+"): "+ this.entrenamientoService.salidaRed(this.neurona.salidaSoma[i], this.entrenamiento.fActivacion));
        this.entrenamiento.errorLineal.push(parseFloat((x[i] - this.neurona.salidaSoma[i]).toFixed(5)));

      }

      console.log("Error Lineal: "); console.log(this.entrenamiento.errorLineal);

      let suma: number = 0;
      for (let i = 0; i < this.neurona.nSalidas; i++) {

        suma +=  this.entrenamiento.errorLineal[i] /this.neurona.nSalidas;
      }

      suma = Math.abs(suma);

      this.entrenamiento.errorPatron[patron] = suma;

      console.log("Error Patron: "); console.log(this.entrenamiento.errorPatron[patron]);

      this.vectorPesos = this.entrenamientoService.ajustarPesos(
        patron,this.matrizSalidas,this.neurona, this.vectorPesos, this.entrenamiento);

        console.log("Nuevos Pesos: "); console.log(this.vectorPesos);

      this.umbrales = this.entrenamientoService.ajustarUmbrales(
       this.umbrales, this.entrenamiento, patron);

       console.log("Nuevos Umbrales: "); console.log(this.umbrales);


    }



    this.entrenamiento.errorRMS = this.entrenamientoService.calcularErrorERM(
      this.entrenamiento.errorPatron, this.neurona.nPatrones);

  }


// Grafica

  view: any[] = [700, 300];
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'interaciones';
  yAxisLabel: string = 'Error MRS';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };



  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }


  // Data Grafica

  data = [
    {
      name: 'Error RMS vs Iteraciones',
      series: [
      ],
    },
  ];


}
